<?php

require ('Vue/main.php');
require ('Vue/form.php');
require ('Vue/fiche.php');
require ('Class/animal.php');

$choix='accueil';

if (isset($_REQUEST['choix'])){
	$choix = $_REQUEST['choix'];			
}

switch ($choix){

	case 'accueil': afficherDebut('Bienvenue à l\'accueil');
					afficherFiche();
	break;

	case 'ajouter': afficherDebut('Formulaire d\'ajout');
					afficherFormulaire();					
	break;

	case 'enregistrer': 
					if ($_REQUEST['id'] != ""){
						$animal= new Animal($_REQUEST['id']);
						$animal->setFamille($_REQUEST['famille']);
						$animal->setEspece($_REQUEST['espece']);
						$animal->setNom($_REQUEST['nom']);
						$animal->setStatut($_REQUEST['statut']);
						$animal->setNombre($_REQUEST['nombre']);
						$animal->setDescription($_REQUEST['description']);
						$animal->setPhoto($_REQUEST['photo']);
						$animal->update();
						header('location: index.php?choix=accueil');//redirection en fonction du case									
					}

					else {						
						$test= new Animal(null,$_REQUEST['famille'],$_REQUEST['espece'],$_REQUEST['nom'],$_REQUEST['statut'],$_REQUEST['nombre'],$_REQUEST['description'],$_REQUEST['photo']);
						$test->save();					
						header('location: index.php?choix=accueil');
					}
	break;

	case 'supprimer':
					if(isset($_REQUEST['id']) and $_REQUEST['id']!=""){
					 	$animal= new Animal($_REQUEST['id']);
					 	$animal->delete();
					 	header('location: index.php?choix=accueil');
					}
	break;

	case 'modifier':
					$animal = new Animal($_REQUEST['id']);
					afficherDebut("Vous pouvez modifier la fiche");
					afficherFormulaire($animal);
	break;
}

afficherFin();

?>