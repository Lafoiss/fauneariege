-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 17 Mai 2019 à 16:20
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `FauneAriege`
--

-- --------------------------------------------------------

--
-- Structure de la table `Animal`
--

CREATE TABLE `Animal` (
  `id` int(11) NOT NULL,
  `famille` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `espece` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statut` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` int(11) DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Animal`
--

INSERT INTO `Animal` (`id`, `famille`, `espece`, `nom`, `statut`, `nombre`, `description`, `photo`) VALUES
(4, 'Mustélidés', 'Belette', 'Belette d\'Europe', 'Sauvage', 100, 'La Belette d\'Europe (Mustela nivalis), aussi connue sous les noms de Belette pygmée1, Petite belette, ou tout simplement Belette, est le plus petit mammifère de la famille des mustélidés et constitue également le plus petit mammifère carnivore d\'Europe avec une taille d\'environ 20 cm pour moins d\'une centaine de grammes seulement.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Mustela_nivalis_-British_Wildlife_Centre-4.jpg/800px-Mustela_nivalis_-British_Wildlife_Centre-4.jpg'),
(5, 'Equidés', 'Chevale', 'Merens', 'Domestique', 5000, 'Le cheval de Mérens, Mérens ou mérengais, encore parfois nommé poney ariégeois, est une race française de petits chevaux de selle et de trait léger rustiques à la robe noire. Il est originaire de la vallée de l\'Ariège.', 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Nickel_de_Vives.jpg/280px-Nickel_de_Vives.jpg'),
(6, 'Rapace', 'Chouette', 'Chouette hulotte', 'Sauvage', 50, 'Surnommée également le chat-huant , la hulotte est la plus connue des rapaces nocturnes. Corps trapu, tacheté avec des couleurs pouvant varier du gris au brun roux. Grosse tête arrondie avec en son centre deux grands yeux noirs séparés par un triangle dont la base part du haut du crâne pour rejoindre le bec.', 'http://www.oiseaux.net/photos/stephan.dubois/images/id/chouette.hulotte.stdu.1p.jpg'),
(12, 'Equidés', 'Ane', 'Baudet', 'Sauvage', 75, 'animal à poil laineux', 'http://www.assoadada.fr/wp-content/uploads/2017/07/GASPARD_AA-5.jpg'),
(17, 'Ursidés', 'Ours', 'Ours brun', 'Sauvage', 29, 'Il est présent dans les Pyrénées depuis 250 000 ans.Il ne subsiste plus en France que dans les Pyrénées, où sa présence divise profondément la population.', 'https://www.ariege.com/images/faune-flore/ours2.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Animal`
--
ALTER TABLE `Animal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Animal`
--
ALTER TABLE `Animal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
