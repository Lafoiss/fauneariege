<?php
function afficherFiche(){
	echo '<div id="fiche">';
		$tabAnimal = Animal::getList();
		foreach($tabAnimal as $animal){
			echo '
					<div id="liste">
						<p class="nom">'.$animal->getNom().'</p>								
						<p><img alt=photo src='.$animal->getPhoto().'></p>
						<p>'.$animal->getFamille().'</p>
						<p>'.$animal->getEspece().'</p>								
						<p>'.$animal->getStatut().'</p>
						<p>'.$animal->getNombre().' en Ariège</p>
						<p class="des">'.$animal->getDescription().'</p>
						<div class=bouton>
							<form id="modifier" action="index.php" method="post">
	                        <input type="hidden" name="id" value="'.$animal->getId().'">
	                        <button type="submit" name="choix" value="modifier">Modifier</button>
	                    	</form>
							<form id="supprimer" action="index.php" method="post">
	                        <input type="hidden" name="id" value="'.$animal->getId().'">
	                        <button type="submit" onclick="return confirm(\'Voulez-vous vraiment supprimer cette fiche?\')" name="choix" value="supprimer">Supprimer</button>                        
	                        </form> 
                        </div>                               
					</div>								
				';
		}				
	echo '</div>';	
}
// onclick= confirm box js avec retour si faux
//<img src pour intégrer la photo avec l'url
//form avec input caché pour récupérer la fiche associée en fonction de son id
?>