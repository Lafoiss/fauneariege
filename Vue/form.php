<?php
function afficherFormulaire($animal=null){
	
	$id=$famille=$espece=$nom=$statut=$nombre=$description=$photo="";
//remplissage du formulaire automatique pour modifier avec getters
	if ($animal!= null){
		$id=$animal->getId();
		$famille=$animal->getFamille();
		$espece=$animal->getEspece();
		$nom=$animal->getNom();
		$statut=$animal->getStatut();
		$nombre=$animal->getNombre();
		$description=$animal->getDescription();
		$photo=$animal->getPhoto();
	}
	echo '
		<main>		
			<div id=form>
				<form action="index.php" method="get">
			
					<input type="hidden" name="id" value="'.$id.'"/>
					<div>
						<p><label>Famille : </label></p>
						<input type="text" name="famille" value="'.$famille.'" required />
					</div>

					<div>
						<p><label>Espèce : </label></p>
						<input type="text" name="espece" value="'.$espece.'" required/>
					</div>
						
					<div>	
						<p><label>Nom :</label></p>
						<input type="text" name="nom" value="'.$nom.'" required/>
					</div>
					
					<div>	
						<p><label>Statut :</label></p>
						<select name="statut" required>
							<option>'.$statut.'</option> 
							<option>Sauvage</option>
							<option>Domestique</option>
							<option>Semi-domestique</option>
						</select>
					</div>

					<div>
						<p><label>Nombre :</label></p>			
						<input type="number" name="nombre" value="'.$nombre.'" required/>
					</div>

					<div>
						<p><label>Description :</label></p>
						<textarea name="description" cols="25"  rows="5" maxlength="512" placeholder="512 caractères maximum"  required>'.$description.'</textarea>
					</div>

					<div>
						<p><label>URL d\'une photo :</label></p>
						<input type="text" name="photo" value="'.$photo.'">
					</div>
					
					<div>	
						<button type="submit" name="choix" value="enregistrer">Enregistrer</button>
					</div>
				</form>				
			</div>		
		</main>';
}
?>