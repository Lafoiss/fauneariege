<?php
class Animal {

	protected $id;

	protected $famille;
	protected $espece;
	protected $nom;
	protected $statut;
	protected $nombre;
	protected $description;
	protected $photo;
	
	function __construct($id, $f=null, $e=null, $n=null, $s=null, $no=null, $d=null, $p=null){
		if ($id==null){			
			$this->famille= $f;
			$this->espece= $e;
			$this->nom= $n;
			$this->statut= $s;
			$this->nombre= $no;
			$this->description= $d;
			$this->photo= $p;
		}
		else{
			$this->load($id);
		}
	}

	public function getId(){return $this->id;}
	public function getFamille(){return $this->famille;}
	public function getEspece(){return $this->espece;}
	public function getNom(){return $this->nom;}
	public function getStatut(){return $this->statut;}
	public function getNombre(){return $this->nombre;}
	public function getDescription(){return $this->description;}
	public function getPhoto(){return $this->photo;}

	public function setFamille($f) {$this->famille=$f;}	
	public function setEspece($e){ $this->espece=$e;}
	public function setNom($n){ $this->nom=$n;}
	public function setStatut($s){ $this->statut=$s;}
	public function setNombre($no){ $this->nombre=$no;}
	public function setDescription($d){ $this->description=$d;}
	public function setPhoto($p){ $this->photo=$p;}


	public function save(){
		require ('Bdd/connect.php');
		$req = $bdd->prepare('INSERT INTO Animal(famille,espece,nom,statut,nombre,description,photo) values(?,?,?,?,?,?,?)');
		$req->bindParam(1,$this->famille);
		$req->bindParam(2,$this->espece);
		$req->bindParam(3,$this->nom);
		$req->bindParam(4,$this->statut);
		$req->bindParam(5,$this->nombre, PDO::PARAM_INT);
		$req->bindParam(6,$this->description);
		$req->bindParam(7,$this->photo);
		
		if($req->execute()==false){
			echo 't\'es nulle 1';
			die;
		}
		else{
			$this->id=$bdd->lastInsertId();
		}
	}

	protected function load($id){

		require ('Bdd/connect.php');
		$req = $bdd->prepare('SELECT * FROM Animal WHERE id=?');
		$req->bindParam(1,$id);
		if($req->execute()==false){
			echo 't\'es nulle 2';
			die;
		}
		else{
			$ligne=$req->fetch();
			$this->id = $ligne['id']; //nom des colonnes['']
			$this->famille   = $ligne['famille'];
			$this->espece  = $ligne['espece'];
			$this->nom = $ligne['nom'];
			$this->statut = $ligne['statut'];
			$this->nombre = $ligne['nombre'];
			$this->description = $ligne['description'];
			$this->photo = $ligne['photo'];
		}
	}

	static function getList(){ //classe statique, méthode de classe

		$tabAnimal = array();
		require ('Bdd/connect.php');
		$req = $bdd->prepare('SELECT id FROM Animal ORDER BY id DESC ');
		if($req->execute()==false){
			echo 't\'es nulle 3';
			die;
		}

		while ($ligne=$req->fetch()){
			
			$animal = new Animal($ligne['id']);
			$tabAnimal[]= $animal;
		}
		
		return $tabAnimal;

	}

	public function delete(){
		require('Bdd/connect.php');
		$req= 'DELETE FROM Animal WHERE id = ?';
		$req = $bdd->prepare($requete);
		$req->bindParam(1, $this->id);
		if($req->execute()==false){
			die('erreur :'.$req);
		}
	}

	public function update() {
		require('Bdd/connect.php');
		$req = 'UPDATE Animal SET famille = ?, espece = ?, nom = ?, statut = ?, nombre = ?, description = ?, photo = ? WHERE id = ?';
		$req = $bdd->prepare($req);
		$req->bindParam(1, $this->famille);
		$req->bindParam(2, $this->espece);
		$req->bindParam(3, $this->nom);
		$req->bindParam(4, $this->statut);
		$req->bindParam(5, $this->nombre, PDO::PARAM_INT);
		$req->bindParam(6, $this->description);
		$req->bindParam(7, $this->photo);
		$req->bindParam(8, $this->id, PDO::PARAM_INT);//protection en +
		if($req->execute()==false){
			die('erreur :'.$req);
		}
	}
}
?>

